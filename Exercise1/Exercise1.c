#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>


#define isSpace(charecter) (charecter==' ') || (charecter=='\t') || (charecter=='\n') || (charecter=='\v') || (charecter=='\f') || (charecter=='\r')
#define NUMBER_OF_NAMES 30
#define MAX_NAME_LENGTH 20


char appMemory[NUMBER_OF_NAMES * (MAX_NAME_LENGTH+1)];
char *names[NUMBER_OF_NAMES];
char *nextFreeMemoryLocation = appMemory;

int isSameString(char* ,char*);
char* printRandomName(void);
int existsInNames(char*);
void getNames(void);
void strCopy(char*, char*);
int noWhiteSpaces(char*);
void printAllNamesInMemory(void);


int main()
{
	getNames();
	printAllNamesInMemory();
	printRandomName();
}

char* printRandomName(void) 
{
	srand(time(NULL));
	int randomIndex = rand() % (NUMBER_OF_NAMES);
	printf("%s\n", names[randomIndex]);
}

int isSameString(char* firstString, char* secondString)
{
	unsigned int i = 0;

	if (firstString[i] == NULL || secondString == NULL)
	{
		return 0;
	}

	while (firstString[i] == secondString[i] || firstString[i] == secondString[i] + 32 || firstString[i] + 32 == secondString[i])
	{
		if (firstString[i] == '\0')
		{
			return 1;
		}
		i++;
	}
	return 0;
}

int existsInNames(char* name)
{
	for (int i = 0; i < NUMBER_OF_NAMES; i++)
	{
		if (isSameString(name, names[i]))
		{
			return 1;
		}
	}
	return 0;
}

void getNames()
{
	char name[MAX_NAME_LENGTH+1];
	int i = 0;
	while (i < NUMBER_OF_NAMES)
	{
		gets(name);
		if (!existsInNames(name) && noWhiteSpaces(name))
		{
			strCopy(name, nextFreeMemoryLocation);
			names[i] = nextFreeMemoryLocation;
			nextFreeMemoryLocation += strlen(name)+1;
			i++;
		}
		else
		{
			printf("Name is illigal or already exists. Try again...\n");
		}
	}
}

void strCopy(char *copyFrom, char *copyTo)
{
	int i = 0;
	while (copyFrom[i] != '\0')
	{
		copyTo[i] = copyFrom[i];
		i++;
	}
	copyTo[i] = '\0';
}

int noWhiteSpaces(char* name)
{
	int i = 0;
	while (name[i] != '\0')
	{
		if (isSpace(name[i]))
		{
			return 0;
		}
		i++;
	}
	return 1;
}

void printAllNamesInMemory()
{
	printf("Names in memory:");
	for (int i = 0; i < NUMBER_OF_NAMES; i++)
	{
		printf("%s ", names[i]);
	}
	printf("\n");
}


